$(function () {

	var kode_jenisassetlama = "{{$result->kode_jenisasset}}";

	$('#btn_ubah').on('click', function(){
		$("#btn_hidden").attr("hidden","false").removeAttr("hidden")
		$("input:disabled").removeAttr("disabled")
        $('.selectpicker').prop('disabled', false);
        $('.selectpicker').selectpicker('refresh');
		$('#hidden_ubah').prop('hidden', 'true')
	})

    $('#btn_batal').click(function() {
        location.reload();
    });
    
	$('#btn_simpan').click(function(){

        if ($('#kode_jenisasset').val() == '') {
            swal( "Kesalahan", "Kode Jenis Asset tidak boleh kosong", "error" )
            return
        }  else if ($('#nama_jenisasset').val() == '') {
            swal( "Kesalahan", "Nama Jenis Asset tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_jenisasset")[0])
        uploadfile.append('id', id)
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        location.href = url_jenisasset
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Kesalahan", "Jenis Asset dengan kode: " + $('#kode_jenisasset').val() + " telah ada", "error")
                }
            }
        })
    })

})