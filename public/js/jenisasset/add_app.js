$(function () {
    
    $('#btn_batal').click(function(){
        $('#kode_jenisasset').val('')
        $('#nama_jenisasset').val('')
        $('#kode_kategoriasset').val('pilih').change()
    })

    $('#btn_simpan').click(function(){
        if ($('#kode_jenisasset').val() == '') {
            swal( "Kesalahan", "Kode Jenis Asset tidak boleh kosong", "error" )
            return
        }  else if ($('#nama_jenisasset').val() == '') {
            swal( "Kesalahan", "Nama Jenis Asset tidak boleh kosong", "error" )
            return
        }  else if ($('#kode_kategoriasset')[0].selectedIndex <= 0) {
            swal( "Kesalahan", "Silahkan pilih Kategori Asset terlebih dahulu", "error" )
            return
        }

        var uploadfile = new FormData($("#form_jenisasset")[0])
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        $('#kode_jenisasset').val('')
                        $('#nama_jenisasset').val('')
                        $('#kode_kategoriasset').val('pilih').change()
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Error", "Jenis Asset sudah ada", "error")
                }
            }
        })
    })

})
