$(function () {
    
    $('#btn_batal').click(function(){
        $('#kode_lokasi').val('')
        $('#nama_lokasi').val('')
        $('#keterangan').val('')
    })

    $('#btn_simpan').click(function(){
        if ($('#kode_lokasi').val() == '') {
            swal( "Kesalahan", "Kode Lokasi tidak boleh kosong", "error" )
            return
        }  else if ($('#nama_lokasi').val() == '') {
            swal( "Kesalahan", "Nama Lokasi tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_lokasi")[0])
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        $('#kode_kantor').val('pilih').change()
                        $('#kode_lokasi').val('')
                        $('#nama_lokasi').val('')
                        $('#keterangan').val('')
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Error", "Lokasi sudah ada", "error")
                }
            }
        })
    })

})
