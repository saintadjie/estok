$(function () {

	var kode_lokasilama = "{{$result->kode_lokasi}}";

	$('#btn_ubah').on('click', function(){
		$("#btn_hidden").attr("hidden","false").removeAttr("hidden")
		$("input:disabled").removeAttr("disabled")
        $("textarea:disabled").removeAttr("disabled")
		$('#hidden_ubah').prop('hidden', 'true')
	})

    $('#btn_batal').click(function() {
        location.reload();
    });
    
	$('#btn_simpan').click(function(){

        if ($('#kode_lokasi').val() == '') {
            swal( "Kesalahan", "Kode Lokasi tidak boleh kosong", "error" )
            return
        }  else if ($('#nama_lokasi').val() == '') {
            swal( "Kesalahan", "Nama Lokasi tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_lokasi")[0])
        uploadfile.append('id', id)
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        location.href = url_lokasi
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Kesalahan", "Lokasi dengan kode: " + $('#kode_lokasi').val() + " telah ada", "error")
                }
            }
        })
    })

})