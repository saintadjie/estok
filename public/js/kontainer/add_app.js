$(function () {
    
    $('#btn_batal').click(function(){
        $('#kode_kontainer').val('')
        $('#nama_kontainer').val('')
        $('#keterangan').val('')
        $('#kode_lokasi').val('pilih').change()
        $('#kode_jeniskontainer').val('pilih').change()
    })

    $('#btn_simpan').click(function(){
        if ($('#kode_kontainer').val() == '') {
            swal( "Kesalahan", "Kode Kontainer tidak boleh kosong", "error" )
            return
        }  else if ($('#nama_kontainer').val() == '') {
            swal( "Kesalahan", "Nama Kontainer tidak boleh kosong", "error" )
            return
        }  else if ($('#kode_lokasi')[0].selectedIndex <= 0) {
            swal( "Kesalahan", "Silahkan pilih Lokasi terlebih dahulu", "error" )
            return
        }  else if ($('#kode_jeniskontainer')[0].selectedIndex <= 0) {
            swal( "Kesalahan", "Silahkan pilih Jenis Kontainer terlebih dahulu", "error" )
            return
        }  else if ($('#jumlah_slot').val() == '') {
            swal( "Kesalahan", "Jumlah Slot tidak boleh kosong", "error" )
            return
        } 

        var uploadfile = new FormData($("#form_kontainer")[0])
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        $('#kode_kontainer').val('')
                        $('#nama_kontainer').val('')
                        $('#keterangan').val('')
                        $('#kode_lokasi').val('pilih').change()
                        $('#kode_jeniskontainer').val('pilih').change()
                        $('#jumlah_slot').val('')
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Error", "Kontainer sudah ada", "error")
                }
            }
        })
    })

})
