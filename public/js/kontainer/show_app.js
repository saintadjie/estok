$(function () {

	var kode_kontainerlama = "{{$result->kode_kontainer}}";

	$('#btn_ubah').on('click', function(){
		$("#btn_hidden").attr("hidden","false").removeAttr("hidden")
		$("input:disabled").removeAttr("disabled")
        $('.selectpicker').prop('disabled', false);
        $('.selectpicker').selectpicker('refresh');
        $("textarea:disabled").removeAttr("disabled")
		$('#hidden_ubah').prop('hidden', 'true')
	})

    $('#btn_batal').click(function() {
        location.reload();
    });
    
	$('#btn_simpan').click(function(){
        if ($('#kode_kontainer').val() == '') {
            swal( "Kesalahan", "Kode Kontainer tidak boleh kosong", "error" )
            return
        }  else if ($('#nama_kontainer').val() == '') {
            swal( "Kesalahan", "Nama Kontainer tidak boleh kosong", "error" )
            return
        }  else if ($('#jumlah_slot').val() == '') {
            swal( "Kesalahan", "Jumlah Slot tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_kontainer")[0])
        uploadfile.append('id', id)
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        location.href = url_kontainer
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Kesalahan", "Kontainer dengan kode: " + $('#kode_kontainer').val() + " telah ada", "error")
                }
            }
        })
    })

})