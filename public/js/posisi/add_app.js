$(function () {
    
    $('#btn_batal').click(function(){
        $('#kode_posisi').val('')
        $('#label_posisi').val('')
        $('#keterangan').val('')
        $('#kode_kontainer').val('pilih').change()
    })

    $('#btn_simpan').click(function(){
        if ($('#kode_posisi').val() == '') {
            swal( "Kesalahan", "Kode Posisi tidak boleh kosong", "error" )
            return
        }  else if ($('#label_posisi').val() == '') {
            swal( "Kesalahan", "Label Posisi tidak boleh kosong", "error" )
            return
        }  else if ($('#kode_kontainer')[0].selectedIndex <= 0) {
            swal( "Kesalahan", "Silahkan pilih Kontainer terlebih dahulu", "error" )
            return
        }

        var uploadfile = new FormData($("#form_posisi")[0])
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        $('#kode_posisi').val('')
                        $('#label_posisi').val('')
                        $('#keterangan').val('')
                        $('#kode_kontainer').val('pilih').change()
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Error", "Posisi sudah ada", "error")
                }
            }
        })
    })

})
