$(function () {

	var kode_posisilama = "{{$result->kode_posisi}}";

	$('#btn_ubah').on('click', function(){
		$("#btn_hidden").attr("hidden","false").removeAttr("hidden")
		$("input:disabled").removeAttr("disabled")
        $('.selectpicker').prop('disabled', false);
        $('.selectpicker').selectpicker('refresh');
        $("textarea:disabled").removeAttr("disabled")
		$('#hidden_ubah').prop('hidden', 'true')
	})

    $('#btn_batal').click(function() {
        location.reload();
    });
    
	$('#btn_simpan').click(function(){

        if ($('#kode_posisi').val() == '') {
            swal( "Kesalahan", "Kode Posisi tidak boleh kosong", "error" )
            return
        }  else if ($('#label_posisi').val() == '') {
            swal( "Kesalahan", "Label Posisi tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_posisi")[0])
        uploadfile.append('id', id)
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        location.href = url_posisi
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Kesalahan", "Posisi dengan kode: " + $('#kode_posisi').val() + " telah ada", "error")
                }
            }
        })
    })

})