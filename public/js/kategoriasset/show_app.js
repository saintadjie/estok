$(function () {

	var kode_kategoriassetlama = "{{$result->kode_kategoriasset}}";

	$('#btn_ubah').on('click', function(){
		$("#btn_hidden").attr("hidden","false").removeAttr("hidden")
		$("input:disabled").removeAttr("disabled")
		$('#hidden_ubah').prop('hidden', 'true')
	})

    $('#btn_batal').click(function() {
        location.reload();
    });
    
	$('#btn_simpan').click(function(){

        if ($('#kode_kategoriasset').val() == '') {
            swal( "Kesalahan", "Kode Kategori Asset tidak boleh kosong", "error" )
            return
        }  else if ($('#nama_kategoriasset').val() == '') {
            swal( "Kesalahan", "Nama Kategori Asset tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_kategoriasset")[0])
        uploadfile.append('id', id)
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        location.href = url_kategoriasset
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Kesalahan", "Kategori Asset dengan kode: " + $('#kode_kategoriasset').val() + " telah ada", "error")
                }
            }
        })
    })

})