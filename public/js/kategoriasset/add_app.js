$(function () {
    
    $('#btn_batal').click(function(){
        $('#kode_kategoriasset').val('')
        $('#nama_kategoriasset').val('')
    })

    $('#btn_simpan').click(function(){
        if ($('#kode_kategoriasset').val() == '') {
            swal( "Kesalahan", "Kode Kategori Asset tidak boleh kosong", "error" )
            return
        }  else if ($('#nama_kategoriasset').val() == '') {
            swal( "Kesalahan", "Nama Kategori Asset tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_kategoriasset")[0])
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        $('#kode_kategoriasset').val('')
                        $('#nama_kategoriasset').val('')
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Error", "Kategori Asset sudah ada", "error")
                }
            }
        })
    })

})
