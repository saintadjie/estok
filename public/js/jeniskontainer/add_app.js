$(function () {
    
    $('#btn_batal').click(function(){
        $('#kode_jeniskontainer').val('')
        $('#nama_jeniskontainer').val('')
        $('#keterangan').val('')
    })

    $('#btn_simpan').click(function(){
        if ($('#kode_jeniskontainer').val() == '') {
            swal( "Kesalahan", "Kode Jenis Kontainer tidak boleh kosong", "error" )
            return
        }  else if ($('#nama_jeniskontainer').val() == '') {
            swal( "Kesalahan", "Nama Jenis Kontainer tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_jeniskontainer")[0])
        
        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
            success: function(data) {
                if(data.status == 'OK'){
                    swal({
                        title: "Sukses",
                        text: "Data telah disimpan",
                        type: "success",
                        timer: 3000,
                        showConfirmButton: true
                    },
                    function(){
                        $('#kode_jeniskontainer').val('')
                        $('#nama_jeniskontainer').val('')
                        $('#keterangan').val('')
                    })
                } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                    swal("Error", "Jenis Kontainer sudah ada", "error")
                }
            }
        })
    })

})
