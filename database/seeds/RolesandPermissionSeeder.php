<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesandPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions    
        // Master Data
        Permission::create(['name' => 'Melihat daftar lokasi']);
        Permission::create(['name' => 'Menambah data lokasi']);
        Permission::create(['name' => 'Mengubah data lokasi']);
        Permission::create(['name' => 'Melihat daftar jenis kontainer']);
        Permission::create(['name' => 'Menambah data jenis kontainer']);
        Permission::create(['name' => 'Mengubah data jenis kontainer']);
        Permission::create(['name' => 'Melihat daftar kontainer']);
        Permission::create(['name' => 'Menambah data kontainer']);
        Permission::create(['name' => 'Mengubah data kontainer']);
        Permission::create(['name' => 'Melihat daftar posisi']);
        Permission::create(['name' => 'Menambah data posisi']);
        Permission::create(['name' => 'Mengubah data posisi']);
        Permission::create(['name' => 'Melihat daftar kategori asset']);
        Permission::create(['name' => 'Menambah data kategori asset']);
        Permission::create(['name' => 'Mengubah data kategori asset']);
        Permission::create(['name' => 'Melihat daftar jenis asset']);
        Permission::create(['name' => 'Menambah data jenis asset']);
        Permission::create(['name' => 'Mengubah data jenis asset']);
        Permission::create(['name' => 'Melihat daftar satker']);
        Permission::create(['name' => 'Menambah data satker']);
        Permission::create(['name' => 'Mengubah data satker']);
        Permission::create(['name' => 'Melihat daftar penyedia']);
        Permission::create(['name' => 'Menambah data penyedia']);
        Permission::create(['name' => 'Mengubah data penyedia']);

        // Transaksi

        Permission::create(['name' => 'Melihat daftar jamaah']);

        // Laporan

		Permission::create(['name' => 'Mencetak seluruh Laporan']);
        Permission::create(['name' => 'Mencetak Laporan berdasarkan kantor']);

        // create roles and assign existing permissions
        
        $role = Role::create(['name' => 'SUPERADMIN']);
        $role->givePermissionTo([	'Melihat daftar lokasi', 
        							'Menambah data lokasi', 
        							'Mengubah data lokasi',
        							'Melihat daftar jenis kontainer',
        							'Menambah data jenis kontainer',
							        'Mengubah data jenis kontainer',
							        'Melihat daftar kontainer',
							        'Menambah data kontainer',
							        'Mengubah data kontainer',
							        'Melihat daftar posisi',
							        'Menambah data posisi',
							        'Mengubah data posisi',
                                    'Melihat daftar kategori asset',
                                    'Menambah data kategori asset',
                                    'Mengubah data kategori asset',
                                    'Melihat daftar jenis asset',
                                    'Menambah data jenis asset',
                                    'Mengubah data jenis asset',
                                    'Melihat daftar satker',
                                    'Menambah data satker',
                                    'Mengubah data satker',
                                    'Melihat daftar penyedia',
                                    'Menambah data penyedia',
                                    'Mengubah data penyedia',

        						]);

       	$role = Role::create(['name' => 'ADMIN SUBDIT TPI']);
        $role->givePermissionTo([	'Melihat daftar lokasi', 
        							'Menambah data lokasi', 
        							'Mengubah data lokasi',
        							'Melihat daftar jenis kontainer',
        							'Menambah data jenis kontainer',
							        'Mengubah data jenis kontainer',
							        'Melihat daftar kontainer',
							        'Menambah data kontainer',
							        'Mengubah data kontainer',
							        'Melihat daftar posisi',
							        'Menambah data posisi',
							        'Mengubah data posisi',
        						]);

        $role = Role::create(['name' => 'ADMIN KANIM']);
        $role->givePermissionTo([	'Melihat daftar lokasi', 
        							'Menambah data lokasi', 
        							'Mengubah data lokasi',
        							'Melihat daftar jenis kontainer',
        							'Menambah data jenis kontainer',
							        'Mengubah data jenis kontainer',
							        'Melihat daftar kontainer',
							        'Melihat daftar posisi',
							        'Menambah data posisi',
							        'Mengubah data posisi',
        						]);

        $role = Role::create(['name' => 'PETUGAS']);
        $role->givePermissionTo([	'Melihat daftar lokasi', 
        							'Mengubah data lokasi',
        							'Melihat daftar jenis kontainer',
							        'Melihat daftar kontainer',
							        'Melihat daftar posisi',
        						]);
    }
}
