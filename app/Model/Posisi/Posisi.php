<?php

namespace App\Model\Posisi;

use Illuminate\Database\Eloquent\Model;

class Posisi extends Model
{
    protected $table = "ms_posisi";
    
    protected $fillable = ['kode_posisi', 'label_posisi', 'keterangan'];
}
