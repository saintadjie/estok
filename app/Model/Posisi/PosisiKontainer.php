<?php

namespace App\model\posisi;

use Illuminate\Database\Eloquent\Model;

class PosisiKontainer extends Model
{
    protected $table = "ms_posisi_kontainer";
    
    protected $fillable = ['kode_posisi', 'kode_kontainer'];
}
