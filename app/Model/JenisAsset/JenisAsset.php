<?php

namespace App\Model\JenisAsset;

use Illuminate\Database\Eloquent\Model;

class JenisAsset extends Model
{
    protected $table = "ms_jenisasset";
    
    protected $fillable = ['kode_jenisasset', 'nama_jenisasset'];
}
