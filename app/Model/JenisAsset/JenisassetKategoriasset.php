<?php

namespace App\Model\JenisAsset;

use Illuminate\Database\Eloquent\Model;

class JenisassetKategoriasset extends Model
{
    protected $table = "ms_jenisasset_kategoriasset";
    
    protected $fillable = ['kode_jenisasset', 'kode_kategoriasset'];
}
