<?php

namespace App\Model\JenisKontainer;

use Illuminate\Database\Eloquent\Model;

class JenisKontainer extends Model
{
    protected $table = "ms_jeniskontainer";
    
    protected $fillable = ['kode_jeniskontainer', 'nama_jeniskontainer', 'keterangan'];
}
