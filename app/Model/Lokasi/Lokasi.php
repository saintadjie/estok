<?php

namespace App\Model\Lokasi;

use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
    protected $table = "ms_lokasi";
    
    protected $fillable = ['kode_lokasi', 'nama_lokasi', 'keterangan'];
}
