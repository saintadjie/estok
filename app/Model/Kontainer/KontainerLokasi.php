<?php

namespace App\model\kontainer;

use Illuminate\Database\Eloquent\Model;

class KontainerLokasi extends Model
{
    protected $table = "ms_kontainer_lokasi";
    
    protected $fillable = ['kode_kontainer', 'kode_lokasi'];
}
