<?php

namespace App\model\kontainer;

use Illuminate\Database\Eloquent\Model;

class KontainerJeniskontainer extends Model
{
    protected $table = "ms_kontainer_jeniskontainer";
    
    protected $fillable = ['kode_posisi', 'kode_jeniskontainer'];
}
