<?php

namespace App\Model\Kontainer;

use Illuminate\Database\Eloquent\Model;

class Kontainer extends Model
{
    protected $table = "ms_kontainer";
    
    protected $fillable = ['kode_kontainer', 'nama_kontainer', 'jumlah_slot', 'keterangan', 'kode_lokasi', 'kode_jeniskontainer'];
}
