<?php

namespace App\Model\KategoriAsset;

use Illuminate\Database\Eloquent\Model;

class KategoriAsset extends Model
{
    protected $table = "ms_kategoriasset";
    
    protected $fillable = ['kode_kategoriasset', 'nama_kategoriasset'];
}
