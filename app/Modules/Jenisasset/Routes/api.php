<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/jenisasset', 'middleware' => 'auth:api'], function () {

	Route::group(['prefix' => 'jenisasset'], function () {
    	Route::post('store', 'JenisAssetController@store');
    	Route::post('edit', 'JenisAssetController@edit');
    	Route::post('delete/{id}', 'JenisAssetController@delete');
    });
	
});