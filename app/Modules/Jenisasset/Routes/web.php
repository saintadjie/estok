<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'jenisasset', 'middleware' => 'auth'], function () {
    
    Route::group(['prefix' => 'jenisasset'], function () {

    	Route::get('/', 'JenisAssetController@index')->middleware('permission:Melihat daftar jenis asset');
        Route::get('add', 'JenisAssetController@page_add')->middleware('permission:Menambah data jenis asset');
        Route::get('show/{id}', 'JenisAssetController@page_show')->middleware('permission:Mengubah data jenis asset')->name('account');

    });

});