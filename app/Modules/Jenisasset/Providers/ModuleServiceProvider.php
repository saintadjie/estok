<?php

namespace App\Modules\Jenisasset\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('jenisasset', 'Resources/Lang', 'app'), 'jenisasset');
        $this->loadViewsFrom(module_path('jenisasset', 'Resources/Views', 'app'), 'jenisasset');
        $this->loadMigrationsFrom(module_path('jenisasset', 'Database/Migrations', 'app'), 'jenisasset');
        $this->loadConfigsFrom(module_path('jenisasset', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('jenisasset', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
