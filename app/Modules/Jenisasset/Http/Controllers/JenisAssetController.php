<?php

namespace App\Modules\Jenisasset\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\model\kategoriasset\KategoriAsset;
use App\model\jenisasset\JenisAsset;
use App\model\jenisasset\JenisassetKategoriasset;

use DB;

class JenisAssetController extends Controller
{
    public function index() {
    	$rs         = JenisAsset::select('ms_jenisasset.id', 'ms_jenisasset.kode_jenisasset', 'nama_jenisasset', 'nama_kategoriasset')
    					->join('ms_jenisasset_kategoriasset', 'ms_jenisasset.kode_jenisasset', 'ms_jenisasset_kategoriasset.kode_jenisasset')
    					->join('ms_kategoriasset', 'ms_jenisasset_kategoriasset.kode_kategoriasset', 'ms_kategoriasset.kode_kategoriasset')
                      	->get();
         
        return view('jenisasset::index', ['rs' => $rs]);

    }

    public function page_add(Request $request) {
		$rs         = JenisAsset::select('id', 'kode_jenisasset', 'nama_jenisasset')
                      	->get();

        $rskategori = KategoriAsset::select('kode_kategoriasset', 'nama_kategoriasset', DB::raw("CONCAT(kode_kategoriasset, ' - ', nama_kategoriasset) as display"))
                                ->get();
        
        return view('jenisasset::add', ['rs' => $rs, 'rskategori' => $rskategori]);
	}

	public function store(Request $request) {
		$check 		= JenisAsset::where('kode_jenisasset', $request->kode_jenisasset)->first();

		if (!empty($check)) {

			return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );

		} else {

            $jenisasset 					= new JenisAsset();
			$jenisasset->kode_jenisasset 	= $request->kode_jenisasset;
			$jenisasset->nama_jenisasset 	= $request->nama_jenisasset;
			$jenisasset->save();

			$jenisasset_kategoriasset 						= new JenisassetKategoriasset();
			$jenisasset_kategoriasset->kode_jenisasset 		= $request->kode_jenisasset;
			$jenisasset_kategoriasset->kode_kategoriasset 	= $request->kode_kategoriasset;
			$jenisasset_kategoriasset->save();

			return response()->json(['status' => 'OK']);
		}
	}

	public function page_show($id) {

        $rs         = JenisAsset::findOrfail($id);
        $rskategori = KategoriAsset::select('kode_kategoriasset', 'nama_kategoriasset', DB::raw("CONCAT(kode_kategoriasset, ' - ', nama_kategoriasset) as display"))
                                ->get();

        return view('jenisasset::show', ['rs' => $rs, 'rskategori' => $rskategori]);
    }

    public function edit(Request $request) {

        $kode_jenisasset 		= $request->kode_jenisasset;             
        $kode_jenisassetlama 	= $request->kode_jenisassetlama;

        if ($kode_jenisassetlama   != $kode_jenisasset) {
            $check = JenisAsset::where('kode_jenisasset', $request->kode_jenisasset)->first();

            if (!empty($check)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
            }
        } 

        DB::table('ms_jenisasset_kategoriasset')->where('kode_jenisasset', $kode_jenisassetlama)->delete();

        $jenisasset                    	= JenisAsset::find($request->id);
        $jenisasset->kode_jenisasset 	= $request->kode_jenisasset;
        $jenisasset->nama_jenisasset 	= $request->nama_jenisasset;

        $jenisasset_kategoriasset 						= new JenisassetKategoriasset();
		$jenisasset_kategoriasset->kode_jenisasset 		= $request->kode_jenisasset;
		$jenisasset_kategoriasset->kode_kategoriasset 	= $request->kode_kategoriasset;
		
        $jenisasset->save();
        $jenisasset_kategoriasset->save();

        return response()->json(['status' => 'OK']);
    }

	public function delete($id) {

        $kategoriasset 	= JenisAsset::findOrfail($id);
        $kategoriasset->delete();

        return response()->json(['status' => 'OK']);

    }
}
