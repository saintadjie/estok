<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsJenisassetKategoriassetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_jenisasset_kategoriasset', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_jenisasset');
            $table->string('kode_kategoriasset');
            $table->foreign('kode_jenisasset')->references('kode_jenisasset')->on('ms_jenisasset')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_jenisasset_kategoriasset');
    }
}
