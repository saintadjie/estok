@extends('layouts.layout')
@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <ol class="breadcrumb breadcrumb-bg-indigo">
            <li><a href="{{url('/home')}}"><i class="material-icons">home</i> Home</a></li>
            <li><a href="{{url('/jenisasset/jenisasset')}}"><i class="material-icons">storage</i> Jenis Asset</a></li>
            <li class="active"><i class="material-icons">rate_review</i> Ubah Jenis Asset</li>
        </ol>
		<div class="card">
			<div class="header bg-blue">
				<h2>
					<u>Jenis Asset</u><small>Mengubah Data Jenis Asset</small>
				</h2>
			</div>

			<div class="body">
				<div class="row clearfix">

					<div class="col-md-12">
						<form id="form_jenisasset">
							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
								<br>

								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="kode_jenisasset" name="kode_jenisasset" value="{{$rs->kode_jenisasset}}" disabled>
										<label class="form-label">Kode Jenis Asset</label>
									</div>
								</div>

								<div class="form-group form-float" hidden>
									<div class="form-line">
										<input type="text" class="form-control" id="kode_jenisassetlama" name="kode_jenisassetlama" value="{{$rs->kode_jenisasset}}" disabled>
										<label class="form-label">Kode Jenis Asset</label>
									</div>
								</div>

								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="nama_jenisasset" name="nama_jenisasset" value="{{$rs->nama_jenisasset}}" disabled>
										<label class="form-label">Nama Jenis Asset</label>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-12">
										<label class="form-label">Kategori Asset</label>
										<select class="form-control show-tick selectpicker" data-header="--Pilih Kategori Asset--" style="font-size: 14px;" id="kode_kategoriasset" name="kode_kategoriasset" data-live-search="true" disabled>
											@foreach($rskategori as $rskategori)
			                                    <option value="{{$rskategori->kode_kategoriasset}}" @if($rskategori->kode_kategoriasset == $rskategori->kode_kategoriasset) selected @endif>{{$rskategori->display}}</option>
			                                @endforeach
										</select>
									</div>
								</div>
								<br/>

								<div class="row clearfix" id="hidden_ubah">
									<div class="col-md-12">
										<div class="pull-right">
											<button type="button" class="btn bg-cyan waves-effect" id="btn_ubah"><i class="material-icons">edit</i>&nbsp;Ubah Data</button>
										</div>
									</div>
								</div>

								<div class="row clearfix" hidden="true" id="btn_hidden">
									<div class="col-md-12">
										<div class="pull-right">
											<button type="button" class="btn bg-cyan waves-effect" id="btn_simpan"><i class="material-icons">save</i>&nbsp;Simpan Data</button>
											<button type="button" class="btn bg-orange waves-effect" id="btn_batal"><i class="material-icons">clear</i>&nbsp;Batal</button>
										</div>
									</div>
								</div>

							</div>	

							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>		

						</form>	
					</div>

				</div>
			</div>
			
		</div>
	</div>

</div>
@push('script-footer')
<script src="{{url('js/jenisasset/show_app.js')}}"></script>

<script type="text/javascript">
	var id = "{{$rs->id}}"
	var url_api = "{{url('api/v1/jenisasset/jenisasset/edit')}}"
	var url_jenisasset = "{{url('/jenisasset/jenisasset/')}}"

</script>
@endpush
@endsection

