@extends('layouts.layout')
@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<ol class="breadcrumb breadcrumb-bg-indigo">
            <li><a href="{{url('/home')}}"><i class="material-icons">home</i> Home</a></li>
            <li><a href="{{url('/jenisasset/jenisasset')}}"><i class="material-icons">storage</i> Jenis Asset</a></li>
            <li class="active"><i class="material-icons">library_add</i> Tambah Jenis Asset</li>
        </ol>
		<div class="card">
			<div class="header bg-blue">
				<h2>
					<u>Jenis Asset</u><small>Menambahkan Data Jenis Asset</small>
				</h2>
			</div>

			<div class="body">
				<div class="row clearfix">

					<div class="col-md-12">
						<form id="form_jenisasset">
							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
								
								<br/>

								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="kode_jenisasset" name="kode_jenisasset">
										<label class="form-label">Kode Jenis Asset</label>
									</div>
								</div>

								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="nama_jenisasset" name="nama_jenisasset">
										<label class="form-label">Nama Jenis Asset</label>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-12">
										<label class="form-label">Kategori Asset</label>
										<select class="form-control show-tick" style="font-size: 14px;" id="kode_kategoriasset" name="kode_kategoriasset" data-live-search="true">
											<option value="pilih" disabled selected>-- Pilih Kategori Asset --</option>
					                        @foreach($rskategori as $rskategori)
					                            <option value="{{$rskategori->kode_kategoriasset}}" data-areaname="{{$rskategori->nama_kategoriasset}}">{{$rskategori->display}}</option>
					                        @endforeach 
										</select>
									</div>
								</div>
								<br/>

								<div class="row clearfix">
									<div class="col-md-12">
										<div class="pull-right">
											<button type="button" class="btn bg-cyan waves-effect" id="btn_simpan"><i class="material-icons">save</i><span>&nbsp;Simpan Data</span></button>
											<button type="button" class="btn bg-orange waves-effect" id="btn_batal"><i class="material-icons">clear</i><span>&nbsp;Batal</span></button>
										</div>
									</div>
								</div>

							</div>	

							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>		

						</form>	
					</div>

				</div>
			</div>
			
		</div>
	</div>

</div>
@push('script-footer')
<script src="{{url('js/jenisasset/add_app.js')}}"></script>

<script type="text/javascript">
	var url_api = "{{url('api/v1/jenisasset/jenisasset/store')}}"
</script>

@endpush
@endsection

