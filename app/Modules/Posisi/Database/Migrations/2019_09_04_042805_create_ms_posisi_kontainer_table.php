<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsPosisiKontainerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_posisi_kontainer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_posisi');
            $table->string('kode_kontainer');
            $table->foreign('kode_posisi')->references('kode_posisi')->on('ms_posisi')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_posisi_kontainer');
    }
}
