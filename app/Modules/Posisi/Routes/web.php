<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'posisi', 'middleware' => 'auth'], function () {
    
    Route::group(['prefix' => 'posisi'], function () {

    	Route::get('/', 'PosisiController@index')->middleware('permission:Melihat daftar posisi');
        Route::get('add', 'PosisiController@page_add')->middleware('permission:Menambah data posisi');
        Route::get('show/{id}', 'PosisiController@page_show')->middleware('permission:Mengubah data posisi')->name('account');
        
    });

});
