<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/posisi', 'middleware' => 'auth:api'], function () {

	Route::group(['prefix' => 'posisi'], function () {
    	Route::post('store', 'PosisiController@store');
    	Route::post('edit', 'PosisiController@edit');
    	Route::post('delete/{id}', 'PosisiController@delete');
    });
	
});
