<?php

namespace App\Modules\Posisi\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('posisi', 'Resources/Lang', 'app'), 'posisi');
        $this->loadViewsFrom(module_path('posisi', 'Resources/Views', 'app'), 'posisi');
        $this->loadMigrationsFrom(module_path('posisi', 'Database/Migrations', 'app'), 'posisi');
        $this->loadConfigsFrom(module_path('posisi', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('posisi', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
