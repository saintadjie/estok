<?php

namespace App\Modules\Posisi\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\model\posisi\Posisi;
use App\model\posisi\PosisiKontainer;
use App\model\kontainer\Kontainer;
use App\model\kontainer\KontainerJeniskontainer;
use App\model\kontainer\KontainerLokasi;

use DB;

class PosisiController extends Controller
{
    public function index() {
    	$rs         = Posisi::select('ms_posisi.id', 'ms_posisi.kode_posisi', 'label_posisi', 'nama_lokasi', 'nama_jeniskontainer', 'nama_kontainer', 'ms_posisi.keterangan')
    					->join('ms_posisi_kontainer', 'ms_posisi.kode_posisi', 'ms_posisi_kontainer.kode_posisi')
                        ->join('ms_kontainer', 'ms_posisi_kontainer.kode_kontainer', 'ms_kontainer.kode_kontainer')
                        ->join('ms_kontainer_jeniskontainer', 'ms_kontainer.kode_kontainer', 'ms_kontainer_jeniskontainer.kode_kontainer')
                        ->join('ms_jeniskontainer', 'ms_kontainer_jeniskontainer.kode_jeniskontainer', 'ms_jeniskontainer.kode_jeniskontainer')
                        ->join('ms_kontainer_lokasi', 'ms_kontainer.kode_kontainer', 'ms_kontainer_lokasi.kode_kontainer')
                        ->join('ms_lokasi', 'ms_kontainer_lokasi.kode_lokasi', 'ms_lokasi.kode_lokasi')
                      	->get();
         
        return view('posisi::index', ['rs' => $rs]);

    }

    public function page_add(Request $request) {
		$rs                 = Posisi::select('id', 'kode_posisi', 'label_posisi', 'keterangan')
                      	         ->get();

        $rskontainer 		= Kontainer::select('kode_kontainer', 'nama_kontainer', DB::raw("CONCAT(kode_kontainer, ' - ', nama_kontainer) as display"))
                                ->get();
        
        return view('posisi::add', ['rs' => $rs, 'rskontainer' => $rskontainer]);
	}

	public function store(Request $request) {
		$check 		= Posisi::where('kode_posisi', $request->kode_posisi)->first();

		if (!empty($check)) {

			return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );

		} else {

            $posisi 					= new Posisi();
			$posisi->kode_posisi 		= $request->kode_posisi;
			$posisi->label_posisi 		= $request->label_posisi;
            $posisi->keterangan 		= $request->keterangan;
			$posisi->save();

			$posisikontainer 					= new PosisiKontainer();
			$posisikontainer->kode_posisi 		= $request->kode_posisi;
			$posisikontainer->kode_kontainer 	= $request->kode_kontainer;
			$posisikontainer->save();

			return response()->json(['status' => 'OK']);
		}
	}

	public function page_show($id) {

        $rs         		= Posisi::findOrfail($id);

        $rskontainer 		= Kontainer::select('kode_kontainer', 'nama_kontainer', DB::raw("CONCAT(kode_kontainer, ' - ', nama_kontainer) as display"))
                                ->get();

        return view('posisi::show', ['rs' => $rs, 'rskontainer' => $rskontainer]);
    }

    public function edit(Request $request) {

        $kode_posisi 				= $request->kode_posisi;             
        $kode_posisilama 			= $request->kode_posisilama;

        if ($kode_posisilama   != $kode_posisi) {
            $check      = Posisi::where('kode_posisi', $request->kode_posisi)->first();

            if (!empty($check)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
            }
        } 

        DB::table('ms_posisi_kontainer')->where('kode_posisi', $kode_posisilama)->delete();

        $posisi 					= Posisi::find($request->id);
        $posisi->kode_posisi 		= $request->kode_posisi;
		$posisi->label_posisi 		= $request->label_posisi;
        $posisi->keterangan 		= $request->keterangan;

        $posisikontainer                    = new PosisiKontainer();
        $posisikontainer->kode_posisi       = $request->kode_posisi;
        $posisikontainer->kode_kontainer    = $request->kode_kontainer;

		$posisi->save();
        $posisikontainer->save();

        return response()->json(['status' => 'OK']);
    }

	public function delete($id) {

        $posisi 							= Posisi::findOrfail($id);
        $posisi->delete();

        return response()->json(['status' => 'OK']);

    }
}
