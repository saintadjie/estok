<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/lokasi', 'middleware' => 'auth:api'], function () {

	Route::group(['prefix' => 'lokasi'], function () {
    	Route::post('store', 'LokasiController@store');
    	Route::post('edit', 'LokasiController@edit');
    	Route::post('delete/{id}', 'LokasiController@delete');
    });
	
});