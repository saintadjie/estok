<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'lokasi', 'middleware' => 'auth'], function () {
    
    Route::group(['prefix' => 'lokasi'], function () {

    	Route::get('/', 'LokasiController@index')->middleware('permission:Melihat daftar lokasi');
        Route::get('add', 'LokasiController@page_add')->middleware('permission:Menambah data lokasi');
        Route::get('show/{id}', 'LokasiController@page_show')->middleware('permission:Mengubah data lokasi')->name('account');

    });

});