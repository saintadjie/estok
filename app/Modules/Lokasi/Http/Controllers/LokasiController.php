<?php

namespace App\Modules\Lokasi\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\model\lokasi\Lokasi;
use App\model\kontainer\KontainerLokasi;

class LokasiController extends Controller
{
    public function index() {
    	$rs         = Lokasi::select('id', 'kode_lokasi', 'nama_lokasi', 'keterangan')
                      	->get();
         
        return view('lokasi::index', ['rs' => $rs]);

    }

    public function page_add(Request $request) {
		$rs         = Lokasi::select('id', 'kode_lokasi', 'nama_lokasi', 'keterangan')
                      	->get();
        
        return view('lokasi::add', ['rs' => $rs]);
	}

	public function store(Request $request) {
		$check 		= Lokasi::where('kode_lokasi', $request->kode_lokasi)->first();

		if (!empty($check)) {

			return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );

		} else {

            $lokasi 					= new Lokasi();
			$lokasi->kode_lokasi 		= $request->kode_lokasi;
			$lokasi->nama_lokasi 		= $request->nama_lokasi;
			$lokasi->keterangan 		= $request->keterangan;
			$lokasi->save();

			return response()->json(['status' => 'OK']);
		}
	}

	public function page_show($id) {

        $rs         = Lokasi::findOrfail($id);

        return view('lokasi::show', ['rs' => $rs]);
    }

    public function edit(Request $request) {

        $kode_lokasi               = $request->kode_lokasi;             
        $kode_lokasilama           = $request->kode_lokasilama;

        if ($kode_lokasilama   != $kode_lokasi) {
            $check      = Lokasi::where('kode_lokasi', $request->kode_lokasi)->first();

            if (!empty($check)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
            }
        }

        $lokasi                    = Lokasi::find($request->id);
        $lokasi->kode_lokasi       = $request->kode_lokasi;
        $lokasi->nama_lokasi       = $request->nama_lokasi;
        $lokasi->keterangan        = $request->keterangan;
        $lokasi->save();

        KontainerLokasi::where('kode_lokasi', $kode_lokasilama)
            ->update(['kode_lokasi' => $kode_lokasi]);

        return response()->json(['status' => 'OK']);
    }

	public function delete($id) {

        $lokasi 	= Lokasi::findOrfail($id);
        $lokasi->delete();

        return response()->json(['status' => 'OK']);

    }
}
