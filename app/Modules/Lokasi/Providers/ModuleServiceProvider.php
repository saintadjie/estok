<?php

namespace App\Modules\Lokasi\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('lokasi', 'Resources/Lang', 'app'), 'lokasi');
        $this->loadViewsFrom(module_path('lokasi', 'Resources/Views', 'app'), 'lokasi');
        $this->loadMigrationsFrom(module_path('lokasi', 'Database/Migrations', 'app'), 'lokasi');
        $this->loadConfigsFrom(module_path('lokasi', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('lokasi', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
