<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'kategoriasset', 'middleware' => 'auth'], function () {
    
    Route::group(['prefix' => 'kategoriasset'], function () {

    	Route::get('/', 'KategoriAssetController@index')->middleware('permission:Melihat daftar kategori asset');
        Route::get('add', 'KategoriAssetController@page_add')->middleware('permission:Menambah data kategori asset');
        Route::get('show/{id}', 'KategoriAssetController@page_show')->middleware('permission:Mengubah data kategori asset')->name('account');

    });

});