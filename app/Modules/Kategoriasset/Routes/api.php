<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/kategoriasset', 'middleware' => 'auth:api'], function () {

	Route::group(['prefix' => 'kategoriasset'], function () {
    	Route::post('store', 'KategoriAssetController@store');
    	Route::post('edit', 'KategoriAssetController@edit');
    	Route::post('delete/{id}', 'KategoriAssetController@delete');
    });
	
});
