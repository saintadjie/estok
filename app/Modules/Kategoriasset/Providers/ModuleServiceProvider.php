<?php

namespace App\Modules\Kategoriasset\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('kategoriasset', 'Resources/Lang', 'app'), 'kategoriasset');
        $this->loadViewsFrom(module_path('kategoriasset', 'Resources/Views', 'app'), 'kategoriasset');
        $this->loadMigrationsFrom(module_path('kategoriasset', 'Database/Migrations', 'app'), 'kategoriasset');
        $this->loadConfigsFrom(module_path('kategoriasset', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('kategoriasset', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
