@extends('layouts.layout')
@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<ol class="breadcrumb breadcrumb-bg-indigo">
            <li><a href="{{url('/home')}}"><i class="material-icons">home</i> Home</a></li>
            <li><a href="{{url('/kategoriasset/kategoriasset')}}"><i class="material-icons">storage</i> Kategori Asset</a></li>
            <li class="active"><i class="material-icons">library_add</i> Tambah Kategori Asset</li>
        </ol>
		<div class="card">
			<div class="header bg-blue">
				<h2>
					<u>Kategori Asset</u><small>Menambahkan Data Kategori Asset</small>
				</h2>
			</div>

			<div class="body">
				<div class="row clearfix">

					<div class="col-md-12">
						<form id="form_kategoriasset">
							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
								
								<br/>

								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="kode_kategoriasset" name="kode_kategoriasset">
										<label class="form-label">Kode Kategori Asset</label>
									</div>
								</div>

								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="nama_kategoriasset" name="nama_kategoriasset">
										<label class="form-label">Nama Kategori Asset</label>
									</div>
								</div>
								<br/>

								<div class="row clearfix">
									<div class="col-md-12">
										<div class="pull-right">
											<button type="button" class="btn bg-cyan waves-effect" id="btn_simpan"><i class="material-icons">save</i><span>&nbsp;Simpan Data</span></button>
											<button type="button" class="btn bg-orange waves-effect" id="btn_batal"><i class="material-icons">clear</i><span>&nbsp;Batal</span></button>
										</div>
									</div>
								</div>

							</div>	

							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>		

						</form>	
					</div>

				</div>
			</div>
			
		</div>
	</div>

</div>
@push('script-footer')
<script src="{{url('js/kategoriasset/add_app.js')}}"></script>

<script type="text/javascript">
	var url_api = "{{url('api/v1/kategoriasset/kategoriasset/store')}}"
</script>

@endpush
@endsection

