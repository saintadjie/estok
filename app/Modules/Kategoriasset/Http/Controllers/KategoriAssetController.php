<?php

namespace App\Modules\Kategoriasset\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\model\kategoriasset\KategoriAsset;
use App\model\jenisasset\JenisassetKategoriasset;

class KategoriAssetController extends Controller
{
    public function index() {
    	$rs         = KategoriAsset::select('id', 'kode_kategoriasset', 'nama_kategoriasset')
                      	->get();
         
        return view('kategoriasset::index', ['rs' => $rs]);

    }

    public function page_add(Request $request) {
		$rs         = KategoriAsset::select('id', 'kode_kategoriasset', 'nama_kategoriasset')
                      	->get();
        
        return view('kategoriasset::add', ['rs' => $rs]);
	}

	public function store(Request $request) {
		$check 		= KategoriAsset::where('kode_kategoriasset', $request->kode_kategoriasset)->first();

		if (!empty($check)) {

			return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );

		} else {

            $kategoriasset 						= new KategoriAsset();
			$kategoriasset->kode_kategoriasset 	= $request->kode_kategoriasset;
			$kategoriasset->nama_kategoriasset 	= $request->nama_kategoriasset;
			$kategoriasset->save();

			return response()->json(['status' => 'OK']);
		}
	}

	public function page_show($id) {

        $rs         = KategoriAsset::findOrfail($id);

        return view('kategoriasset::show', ['rs' => $rs]);
    }

    public function edit(Request $request) {

        $kode_kategoriasset 		= $request->kode_kategoriasset;             
        $kode_kategoriassetlama 	= $request->kode_kategoriassetlama;

        if ($kode_kategoriassetlama   != $kode_kategoriasset) {
            $check = KategoriAsset::where('kode_kategoriasset', $request->kode_kategoriasset)->first();

            if (!empty($check)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
            }
        } 

        $kategoriasset                    	= KategoriAsset::find($request->id);
        $kategoriasset->kode_kategoriasset 	= $request->kode_kategoriasset;
        $kategoriasset->nama_kategoriasset 	= $request->nama_kategoriasset;
        $kategoriasset->save();

        JenisassetKategoriasset::where('kode_kategoriasset', $kode_kategoriassetlama)
            ->update(['kode_kategoriasset' => $kode_kategoriasset]);

        return response()->json(['status' => 'OK']);
    }

	public function delete($id) {

        $kategoriasset 	= KategoriAsset::findOrfail($id);
        $kategoriasset->delete();

        return response()->json(['status' => 'OK']);

    }
}
