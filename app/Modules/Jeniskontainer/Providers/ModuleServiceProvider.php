<?php

namespace App\Modules\Jeniskontainer\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('jeniskontainer', 'Resources/Lang', 'app'), 'jeniskontainer');
        $this->loadViewsFrom(module_path('jeniskontainer', 'Resources/Views', 'app'), 'jeniskontainer');
        $this->loadMigrationsFrom(module_path('jeniskontainer', 'Database/Migrations', 'app'), 'jeniskontainer');
        $this->loadConfigsFrom(module_path('jeniskontainer', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('jeniskontainer', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
