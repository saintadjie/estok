<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'jeniskontainer', 'middleware' => 'auth'], function () {
    
    Route::group(['prefix' => 'jeniskontainer'], function () {

    	Route::get('/', 'JenisKontainerController@index')->middleware('permission:Melihat daftar jenis kontainer');
        Route::get('add', 'JenisKontainerController@page_add')->middleware('permission:Menambah data jenis kontainer');
        Route::get('show/{id}', 'JenisKontainerController@page_show')->middleware('permission:Mengubah data jenis kontainer')->name('account');

    });

});