<?php

namespace App\Modules\Jeniskontainer\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\model\jeniskontainer\JenisKontainer;
use App\model\kontainer\KontainerJeniskontainer;

class JenisKontainerController extends Controller
{
    public function index() {
    	$rs         = JenisKontainer::select('id', 'kode_jeniskontainer', 'nama_jeniskontainer', 'keterangan')
                      	->get();
         
        return view('jeniskontainer::index', ['rs' => $rs]);

    }

    public function page_add(Request $request) {
		$rs         = JenisKontainer::select('id', 'kode_jeniskontainer', 'nama_jeniskontainer', 'keterangan')
                      	->get();
        
        return view('jeniskontainer::add', ['rs' => $rs]);
	}

	public function store(Request $request) {
		$check 		= JenisKontainer::where('kode_jeniskontainer', $request->kode_jeniskontainer)->first();

		if (!empty($check)) {

			return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );

		} else {

            $jeniskontainer 						= new JenisKontainer();
			$jeniskontainer->kode_jeniskontainer 	= $request->kode_jeniskontainer;
			$jeniskontainer->nama_jeniskontainer 	= $request->nama_jeniskontainer;
			$jeniskontainer->keterangan 			= $request->keterangan;
			$jeniskontainer->save();

			return response()->json(['status' => 'OK']);
		}
	}

	public function page_show($id) {

        $rs         = JenisKontainer::findOrfail($id);

        return view('jeniskontainer::show', ['rs' => $rs]);
    }

    public function edit(Request $request) {

        $kode_jeniskontainer               			= $request->kode_jeniskontainer;             
        $kode_jeniskontainerlama           			= $request->kode_jeniskontainerlama;

        if ($kode_jeniskontainerlama   != $kode_jeniskontainer) {
            $check      = JenisKontainer::where('kode_jeniskontainer', $request->kode_jeniskontainer)->first();

            if (!empty($check)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
            }
        } 

        $jeniskontainer 							= JenisKontainer::find($request->id);
        $jeniskontainer->kode_jeniskontainer       	= $request->kode_jeniskontainer;
        $jeniskontainer->nama_jeniskontainer       	= $request->nama_jeniskontainer;
        $jeniskontainer->keterangan        			= $request->keterangan;
        $jeniskontainer->save();

        KontainerJeniskontainer::where('kode_jeniskontainer', $kode_jeniskontainerlama)
            ->update(['kode_jeniskontainer' => $kode_jeniskontainer]);

        return response()->json(['status' => 'OK']);
    }

	public function delete($id) {

        $jeniskontainer 							= JenisKontainer::findOrfail($id);
        $jeniskontainer->delete();

        return response()->json(['status' => 'OK']);

    }
}
