<?php

namespace App\Modules\Kontainer\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\model\kontainer\Kontainer;
use App\model\kontainer\KontainerLokasi;
use App\model\kontainer\KontainerJeniskontainer;
use App\model\lokasi\Lokasi;
use App\model\jeniskontainer\JenisKontainer;
use App\model\posisi\Posisi;
use App\model\posisi\PosisiKontainer;

use DB;

class KontainerController extends Controller
{
    public function index() {
    	$rs         = Kontainer::select('ms_kontainer.id', 'ms_kontainer.kode_kontainer', 'nama_kontainer', 'jumlah_slot', 'ms_kontainer.keterangan', 'nama_lokasi', 'nama_jeniskontainer')
                        ->join('ms_kontainer_lokasi', 'ms_kontainer.kode_kontainer', 'ms_kontainer_lokasi.kode_kontainer')
                        ->join('ms_lokasi', 'ms_kontainer_lokasi.kode_lokasi', 'ms_lokasi.kode_lokasi')
                        ->join('ms_kontainer_jeniskontainer', 'ms_kontainer.kode_kontainer', 'ms_kontainer_jeniskontainer.kode_kontainer')
                        ->join('ms_jeniskontainer', 'ms_kontainer_jeniskontainer.kode_jeniskontainer', 'ms_jeniskontainer.kode_jeniskontainer')
                      	->get();
         
        return view('kontainer::index', ['rs' => $rs]);

    }

    public function page_add(Request $request) {
		$rs                 = Kontainer::select('id', 'kode_kontainer', 'nama_kontainer', 'jumlah_slot', 'keterangan')
                      	         ->get();

        $rslokasi           = Lokasi::select('kode_lokasi', 'nama_lokasi', DB::raw("CONCAT(kode_lokasi, ' - ', nama_lokasi) as display"))
                                ->get();

        $rsjeniskontainer   = JenisKontainer::select('kode_jeniskontainer', 'nama_jeniskontainer', DB::raw("CONCAT(kode_jeniskontainer, ' - ', nama_jeniskontainer) as display"))
                                ->get();
        
        return view('kontainer::add', ['rs' => $rs, 'rslokasi' => $rslokasi, 'rsjeniskontainer' => $rsjeniskontainer]);
	}

	public function store(Request $request) {
		$check 		= Kontainer::where('kode_kontainer', $request->kode_kontainer)->first();

		if (!empty($check)) {

			return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );

		} else {

            $kontainer                       = new Kontainer();
			$kontainer->kode_kontainer       = $request->kode_kontainer;
			$kontainer->nama_kontainer 	     = $request->nama_kontainer;
            $kontainer->jumlah_slot          = $request->jumlah_slot;
			$kontainer->keterangan 			 = $request->keterangan;

            $kontainerlokasi                    = new KontainerLokasi();
            $kontainerlokasi->kode_kontainer    = $request->kode_kontainer;
            $kontainerlokasi->kode_lokasi       = $request->kode_lokasi;

            $kontainerjeniskontainer                        = new KontainerJeniskontainer();
            $kontainerjeniskontainer->kode_kontainer        = $request->kode_kontainer;
            $kontainerjeniskontainer->kode_jeniskontainer   = $request->kode_jeniskontainer;

            $kontainer->save();
            $kontainerlokasi->save();
            $kontainerjeniskontainer->save();

			return response()->json(['status' => 'OK']);
		}
	}

	public function page_show($id) {

        $rs                 = Kontainer::findOrfail($id);

        $rslokasi           = Lokasi::select('kode_lokasi', 'nama_lokasi', DB::raw("CONCAT(kode_lokasi, ' - ', nama_lokasi) as display"))
                                ->get();

        $rsjeniskontainer   = JenisKontainer::select('kode_jeniskontainer', 'nama_jeniskontainer', DB::raw("CONCAT(kode_jeniskontainer, ' - ', nama_jeniskontainer) as display"))
                                ->get();

        return view('kontainer::show', ['rs' => $rs, 'rslokasi' => $rslokasi, 'rsjeniskontainer' => $rsjeniskontainer]);
    }

    public function edit(Request $request) {

        $kode_kontainer                         = $request->kode_kontainer;             
        $kode_kontainerlama                     = $request->kode_kontainerlama;

        if ($kode_kontainerlama   != $kode_kontainer) {
            $check      = Kontainer::where('kode_kontainer', $request->kode_kontainer)->first();

            if (!empty($check)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
            }
        }

        DB::table('ms_kontainer_lokasi')->where('kode_kontainer', $kode_kontainerlama)->delete();
        DB::table('ms_kontainer_jeniskontainer')->where('kode_kontainer', $kode_kontainerlama)->delete();


        $kontainer                       = Kontainer::find($request->id);
        $kontainer->kode_kontainer       = $request->kode_kontainer;
        $kontainer->nama_kontainer       = $request->nama_kontainer;
        $kontainer->jumlah_slot          = $request->jumlah_slot;
        $kontainer->keterangan           = $request->keterangan;

        $kontainerlokasi                    = new KontainerLokasi();
        $kontainerlokasi->kode_kontainer    = $request->kode_kontainer;
        $kontainerlokasi->kode_lokasi       = $request->kode_lokasi;

        $kontainerjeniskontainer                        = new KontainerJeniskontainer();
        $kontainerjeniskontainer->kode_kontainer        = $request->kode_kontainer;
        $kontainerjeniskontainer->kode_jeniskontainer   = $request->kode_jeniskontainer;

        $kontainer->save();
        $kontainerlokasi->save();
        $kontainerjeniskontainer->save();

        PosisiKontainer::where('kode_kontainer', $kode_kontainerlama)
            ->update(['kode_kontainer' => $kode_kontainer]);

        return response()->json(['status' => 'OK']);
    }

	public function delete($id) {

        $kontainer 							= Kontainer::findOrfail($id);
        $kontainer->delete();

        return response()->json(['status' => 'OK']);

    }
}
