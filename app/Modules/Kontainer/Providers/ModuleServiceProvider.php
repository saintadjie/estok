<?php

namespace App\Modules\Kontainer\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('kontainer', 'Resources/Lang', 'app'), 'kontainer');
        $this->loadViewsFrom(module_path('kontainer', 'Resources/Views', 'app'), 'kontainer');
        $this->loadMigrationsFrom(module_path('kontainer', 'Database/Migrations', 'app'), 'kontainer');
        $this->loadConfigsFrom(module_path('kontainer', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('kontainer', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
