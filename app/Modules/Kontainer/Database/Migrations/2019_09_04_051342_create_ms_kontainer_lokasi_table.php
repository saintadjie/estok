<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsKontainerLokasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_kontainer_lokasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_kontainer');
            $table->string('kode_lokasi');
            $table->foreign('kode_kontainer')->references('kode_kontainer')->on('ms_kontainer')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_kontainer_lokasi');
    }
}
