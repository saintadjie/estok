@extends('layouts.layout')
@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <ol class="breadcrumb breadcrumb-bg-indigo">
            <li><a href="{{url('/home')}}"><i class="material-icons">home</i> Home</a></li>
            <li><a href="{{url('/kontainer/kontainer')}}"><i class="material-icons">widgets</i> Kontainer</a></li>
            <li class="active"><i class="material-icons">rate_review</i> Ubah Kontainer</li>
        </ol>
		<div class="card">
			<div class="header bg-blue">
				<h2>
					<u>Kontainer</u><small>Mengubah Data Kontainer</small>
				</h2>
			</div>

			<div class="body">
				<div class="row clearfix">

					<div class="col-md-12">
						<form id="form_kontainer">
							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
								<br>

								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="kode_kontainer" name="kode_kontainer" value="{{$rs->kode_kontainer}}" disabled>
										<label class="form-label">Kode Kontainer</label>
									</div>
								</div>

								<div class="form-group form-float" hidden>
									<div class="form-line">
										<input type="text" class="form-control" id="kode_kontainerlama" name="kode_kontainerlama" value="{{$rs->kode_kontainer}}" disabled>
										<label class="form-label">Kode Kontainer</label>
									</div>
								</div>

								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="nama_kontainer" name="nama_kontainer" value="{{$rs->nama_kontainer}}" disabled>
										<label class="form-label">Nama Kontainer</label>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-12">
										<label class="form-label">Lokasi</label>
										<select class="form-control show-tick selectpicker" data-header="--Pilih Lokasi--" style="font-size: 14px;" id="kode_lokasi" name="kode_lokasi" data-live-search="true" disabled>
											@foreach($rslokasi as $rslokasi)
			                                    <option value="{{$rslokasi->kode_lokasi}}" @if($rslokasi->kode_lokasi == $rslokasi->kode_lokasi) selected @endif>{{$rslokasi->display}}</option>
			                                @endforeach
										</select>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-md-12">
										<label class="form-label">Jenis Kontainer</label>
										<select class="form-control show-tick selectpicker" data-header="--Pilih Jenis Kontainer--" style="font-size: 14px;" id="kode_jeniskontainer" name="kode_jeniskontainer" data-live-search="true" disabled>
											@foreach($rsjeniskontainer as $rsjeniskontainer)
			                                    <option value="{{$rsjeniskontainer->kode_jeniskontainer}}" @if($rsjeniskontainer->kode_jeniskontainer == $rsjeniskontainer->kode_jeniskontainer) selected @endif>{{$rsjeniskontainer->display}}</option>
			                                @endforeach
										</select>
									</div>
								</div>

								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" class="form-control" id="jumlah_slot" name="jumlah_slot" value="{{$rs->jumlah_slot}}" disabled>
										<label class="form-label">Jumlah Slot</label>
									</div>
								</div>

								<div class="form-group form-float">
									<div class="form-line">
										<textarea rows="4" class="form-control no-resize"  id="keterangan" name="keterangan" disabled>{{$rs->keterangan}}</textarea>
										<label class="form-label">Keterangan</label>
									</div>
								</div>
								<br/>

								<div class="row clearfix" id="hidden_ubah">
									<div class="col-md-12">
										<div class="pull-right">
											<button type="button" class="btn bg-cyan waves-effect" id="btn_ubah"><i class="material-icons">edit</i>&nbsp;Ubah Data</button>
										</div>
									</div>
								</div>

								<div class="row clearfix" hidden="true" id="btn_hidden">
									<div class="col-md-12">
										<div class="pull-right">
											<button type="button" class="btn bg-cyan waves-effect" id="btn_simpan"><i class="material-icons">save</i>&nbsp;Simpan Data</button>
											<button type="button" class="btn bg-orange waves-effect" id="btn_batal"><i class="material-icons">clear</i>&nbsp;Batal</button>
										</div>
									</div>
								</div>

							</div>	

							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>		

						</form>	
					</div>

				</div>
			</div>
			
		</div>
	</div>

</div>
@push('script-footer')
<script src="{{url('js/kontainer/show_app.js')}}"></script>

<script type="text/javascript">
	var id = "{{$rs->id}}"
	var url_api = "{{url('api/v1/kontainer/kontainer/edit')}}"
	var url_kontainer = "{{url('/kontainer/kontainer/')}}"

</script>
@endpush
@endsection

