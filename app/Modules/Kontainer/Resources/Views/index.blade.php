@extends('layouts.layout')
@section('content')
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<ol class="breadcrumb breadcrumb-bg-indigo">
            <li><a href="{{url('/home')}}"><i class="material-icons">home</i> Home</a></li>
            <li class="active"><i class="material-icons">widgets</i> Kontainer</li>
        </ol>
		<div class="card">
			<div class="body">
				<div>
					<a href="{{url('/kontainer/kontainer/add')}}" id="btn_tambah" class="btn bg-blue waves-effect"><i class="material-icons">add_circle_outline</i>&nbsp;Tambah Data</a>
				</div>
				<hr>
				<div class="panel panel-success">
					<div class="panel-heading bg-indigo">
						Daftar Kontainer
					</div>
					<div class="panel-body table-responsive">
						<table id="tb_kontainer" width="100%" role="grid" class="table table-striped table-bordered table-hover table-responsive">
							<thead class="breadcrumb-bg-blue">
								<tr>
									<th style="text-align: center; color: #fff" class="th_table">Kode Kontainer</th>
									<th style="text-align: center; color: #fff" class="th_table">Nama Kontainer</th>
									<th style="text-align: center; color: #fff" class="th_table">Lokasi</th>
									<th style="text-align: center; color: #fff" class="th_table">Jenis Kontainer</th>
									<th style="text-align: center; color: #fff" class="th_table">Keterangan</th>
									<th style="text-align: center; color: #fff" class="th_table">Aksi</th>
								</tr>
							</thead>
							<tbody id="tbody">
								
								@foreach($rs as $result)
		                        <tr id="{{$result->id}}">
									<td style="text-align: center;">{{ $result->kode_kontainer }}</td>
		                            <td style="text-align: center;">{{ $result->nama_kontainer }}</td>
		                            <td style="text-align: center;">{{ $result->nama_lokasi }}</td>
		                            <td style="text-align: center;">{{ $result->nama_jeniskontainer }}</td>
		                            <td style="text-align: center;">{{ $result->keterangan }}</td>
		                            <td style="text-align: center;">
										<a href="{{url('/kontainer/kontainer/show/'.$result->id)}}" title="EDIT Kontainer">
											<i class="btn btn-xs waves-effect material-icons" id="btn_edit">edit</i>
										</a>
										<i class="btn btn-xs waves-effect material-icons" id="btn_hapus" title="Hapus Data" data-kodekontainer="{{$result->kode_kontainer}}">delete</i>
									</td>
								</tr>
		                        @endforeach

							</tbody>
							
						</table>
					</div>
				</div>
				
			</div>
		</div>
	</div>

</div>
@push('script-footer')
<script src="{{url('js/kontainer/index_app.js')}}"></script>

<script type="text/javascript">
	var url_api = "{{url('api/v1/kontainer/kontainer/delete')}}"
	var url_kontainer = "{{url('/kontainer/kontainer')}}"
</script>
@endpush
@endsection