<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'kontainer', 'middleware' => 'auth'], function () {
    
    Route::group(['prefix' => 'kontainer'], function () {

    	Route::get('/', 'KontainerController@index')->middleware('permission:Melihat daftar kontainer');
        Route::get('add', 'KontainerController@page_add')->middleware('permission:Menambah data kontainer');
        Route::get('show/{id}', 'KontainerController@page_show')->middleware('permission:Mengubah data kontainer')->name('account');

    });

});
